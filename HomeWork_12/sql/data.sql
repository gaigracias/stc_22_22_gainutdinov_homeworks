INSERT INTO driver (first_name, last_name, phone_number, exp, age, has_license, license_category, rating)
VALUES ('Elias', 'Guldberg', 83746476, 8, 70, true, 'B', 5),
       ('Leon', 'Guldberg', '47377476', 5, 63, false, null, 4.7),
       ('Nathan', 'Burger', 37446476, 5, 31, true, 'A', 3.3),
       ('Rasmus', 'Nanchte', '45-564-764', 0, 23, false, null, 0),
       ('Arnol', 'Acht', '343746476', 3, 45, true, 'C', 3.5),
       ('Franz', 'Elster', '74-464-7611', 11, 74, false, null, .11);

INSERT INTO car (model_name, color, car_number, owner_id)
VALUES ('Tesla', 'Gray', 424, 4),
       ('Audi', 'Gray', 634, 2),
       ('Toyota', 'Red', 235, 5),
       ('Ford', 'Yellow', 424, 2),
       ('Audi', 'White', 740, 1);

INSERT INTO trip (start_time, duration, owner_id, car_id)
VALUES ('2020-08-20', age('2020-10-01', '2020-08-20'), 2, 1),
       ('2020-11-25', age('2020-12-30', '2020-11-25'), 4, 3),
       ('2020-09-14', age('2020-10-10', '2020-09-14'), 2, 2),
       ('2020-08-22', age('2020-09-06', '2020-08-22'), 5, 5),
       ('2021-03-15', age('2021-04-10', '2021-03-15'), 4, 3);