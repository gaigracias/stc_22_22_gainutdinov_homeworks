CREATE TABLE customer (
    id bigserial PRIMARY KEY,
    first_name varchar(30) NOT NULL,
    last_name varchar(30) NOT NULL,
    phone_number varchar(30),
    exp int,
    age int,
    has_license bool DEFAULT false,
    license_category varchar(1) DEFAULT NULL,
    rating real
);

CREATE TABLE car (
    id bigserial PRIMARY KEY,
    model_name varchar(30) NOT NULL,
    color varchar(50) NOT NULL,
    car_number varchar(30),
    owner_id int NOT NULL,
    FOREIGN KEY (owner_id) REFERENCES customer(id)
);

CREATE TABLE trip (
    id bigserial PRIMARY KEY,
    start_time date NOT NULL,
    duration interval,
    user_id int NOT NULL,
    car_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES customer(id),
    FOREIGN KEY (car_id) REFERENCES car(id)
);