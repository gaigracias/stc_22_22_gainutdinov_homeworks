package ru.inno.cars.models;

public enum CarColors {
    GRAY,
    RED,
    YELLOW,
    WHITE,
    GREEN
}
