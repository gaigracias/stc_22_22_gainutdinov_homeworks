package ru.inno.cars.repository;

import ru.inno.cars.models.Car;
import ru.inno.cars.models.CarColors;

import java.util.List;

public interface CarRepository {
    List<Car> findAll();

    void insert(String modelName, CarColors color);

    void insert(String modelName, CarColors color, Integer carNumber);

    void insert(String modelName, CarColors color, Integer carNumber, String owner);

    void toTable(List<Car> cars);
}
