package ru.inno.cars.repository;

import ru.inno.cars.app.DataBase;
import ru.inno.cars.models.Car;
import ru.inno.cars.models.CarColors;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CarRepositoryJdbcImpl implements CarRepository {
    @Override
    public List<Car> findAll() {
        String query = "SELECT * FROM car ORDER BY id";
        List<Car> cars = new ArrayList<>();
        try (PreparedStatement stmt = DataBase.connection().prepareStatement(query)){
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Car car = Car.builder()
                        .id(resultSet.getLong("id"))
                        .modelName(resultSet.getString("model_name"))
                        .color(CarColors.valueOf(resultSet.getString("color").toUpperCase()))
                        .carNumber(resultSet.getInt("car_number"))
                        .carOwnerId(resultSet.getInt("customer_id"))
                        .build();

                cars.add(car);
            }
            return cars;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insert(String modelName, CarColors color) {
        insert(modelName, color, null);
    }

    @Override
    public void insert(String modelName, CarColors color, Integer carNumber) {
        insert(modelName, color, carNumber, null);
    }

    @Override
    public void insert(String modelName, CarColors color, Integer carNumber, String owner) {
        String query;
        if (owner != null){
            query = "INSERT INTO car (model_name, color, car_number, customer_id) VALUES (?, ?, ?, (SELECT id FROM customer WHERE first_name=?))";
        } else {
            query = "INSERT INTO car (model_name, color, car_number, customer_id) VALUES (?, ?, ?, ?)";
        }
        try (PreparedStatement stmt = DataBase.connection().prepareStatement(query)){
            stmt.setString(1, modelName);
            stmt.setString(2, color.toString().toLowerCase());
            if (carNumber != null){
                stmt.setInt(3, carNumber);
            } else {
                stmt.setNull(3, Types.INTEGER);
            }
            if (owner != null){
                stmt.setString(4, owner);
            } else {
                stmt.setNull(4, Types.INTEGER);
            }


            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void toTable(List<Car> cars) {
        Iterator<Car> carIterator = cars.iterator();

        Object[] header = {"id", "modelName", "color", "number", "owner_id"};
        System.out.format("%-15s%-15s%-15s%-15s%-15s%n", header);
        while (carIterator.hasNext()){
            Car car = carIterator.next();
            String[] carsData = {car.getId().toString(),
                    car.getModelName().toString(),
                    car.getColor().toString(),
                    car.getCarNumber() == 0 ? "NULL" : car.getCarNumber().toString(),
                    car.getCarOwnerId() == 0 ? "NULL" : car.getCarOwnerId().toString()};
            System.out.format("%-15s%-15s%-15s%-15s%-15s%n", carsData);
        }
    }
}
