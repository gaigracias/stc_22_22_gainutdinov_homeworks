package ru.inno;

import ru.inno.cars.models.CarColors;
import ru.inno.cars.repository.CarRepository;
import ru.inno.cars.repository.CarRepositoryJdbcImpl;

import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var property = System.getProperty("action");
        
        if (property.equals("read")){
            CarRepository carRepository = new CarRepositoryJdbcImpl();
            carRepository.toTable(carRepository.findAll());
        } else if (property.equals("write")) {
            CarRepository carRepository = new CarRepositoryJdbcImpl();
            System.out.println("Car name");
            String scanner = new Scanner(System.in).nextLine();
            System.out.println("Car color");
            CarColors scanner1 = CarColors.valueOf(new Scanner(System.in).nextLine().toUpperCase());

            System.out.println("Do u need Car number?, type Y/N");
            String scanner2 = new Scanner(System.in).nextLine();
            System.out.println("Do u need Car owner name?, type Y/N");
            String scanner3 = new Scanner(System.in).nextLine();
            if ((scanner2.equals("y") || scanner2.equals("Y")) &&
                    (scanner3.equals("y") || scanner3.equals("Y"))){

                    System.out.println("Car number");
                    int scanner4 = Integer.parseInt(new Scanner(System.in).nextLine());
                    System.out.println("Car owner name");
                    String scanner5 = new Scanner(System.in).nextLine();

                carRepository.insert(scanner, scanner1, scanner4, scanner5);
            } else if (scanner2.equals("y") || scanner2.equals("Y")) {

                System.out.println("Car number");
                Integer scanner4 = Integer.parseInt(new Scanner(System.in).nextLine());

                carRepository.insert(scanner, scanner1, scanner4);
            } else if (scanner3.equals("y") || scanner3.equals("Y")) {

                System.out.println("Car owner name");
                String scanner5 = new Scanner(System.in).nextLine();

                carRepository.insert(scanner, scanner1, null, scanner5);
            }

        } else {
            System.exit(0);
        }
    }
}
