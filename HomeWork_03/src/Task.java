public class Task {
    public int countOfLocalMinimum(int[] arr){
        int count = 0;
            for (int i = 0; i < arr.length; i++){
                if (i != 0 && i != arr.length - 1){
                    if (arr[i-1] > arr[i] && arr[i] < arr[i+1]){
                        count++;
                    }
                } else if (i == 0 && arr[i] < arr[i+1]) {
                    count++;
                } else if (i == arr.length - 1 && arr[i-1] > arr[i]){
                    count++;
                }
            }
        return count;
    }
    public static void arrayCount(int[] arr){
        System.out.println("arraySize: " + arr.length);

        for (int i = 0; i < arr.length; i++){
            if (i != arr.length-1){
                System.out.print(arr[i] + ", ");
            } else {
                System.out.print(arr[i] +  "\n");
            }
        }
    }
    public static void main(String[] args){
        int[] arr1 = {3, 3, 27, 1, 43, 3, 20, 12, 444, 20, 15, 1, 0, 13};
        //int[] arr2 = {12, 6, 8, 11, 22, 5, 8, 15, 22, 0};

        arrayCount(arr1);

        Task task = new Task();
        System.out.println("countOfLocalMin: " + task.countOfLocalMinimum(arr1));
    }
}
