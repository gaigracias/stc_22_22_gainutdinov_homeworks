public class Main {
    public static void main(String[] args) {
        double maxWithdrawBalance = 100;
        double maxBankBalance = 200;

        ATM bank = new ATM(maxWithdrawBalance, maxBankBalance);

        assert 0 == bank.withdraw(50);
        assert 30 == bank.deposit(30);
        assert 30 == bank.deposit(30);
        assert 40 == bank.withdraw(40);
        assert 20 == bank.withdraw(30);
        assert 5 == bank.getOperationsCount();
        assert 0 == bank.getBalance();

        assert 50 == bank.deposit(250);
        assert 100 == bank.withdraw(150);
        assert 7 == bank.getOperationsCount();
        assert 100 == bank.getBalance();

        assert 100 == bank.withdraw(100);
        assert 0 == bank.getBalance();

        assert 0 == bank.withdraw(150);
        assert 0 == bank.getBalance();
        assert 0 == bank.withdraw(50);
        assert 0 == bank.getBalance();

        assert 50 == bank.deposit(250);
        assert 200 == bank.getBalance();
        assert 100 == bank.deposit(100);
        assert 300 == bank.deposit(300);
        assert 200 == bank.getBalance();

        assert 100 == bank.withdraw(400);
        assert 100 == bank.getBalance();
        assert 50 == bank.withdraw(50);
        assert 50 == bank.getBalance();

        assert 150 == bank.deposit(150);
        assert 200 == bank.getBalance();
        assert 100 == bank.withdraw(200);
        assert 100 == bank.getBalance();

        assert 100 == bank.deposit(100);
        assert 100 == bank.withdraw(100);
        assert 100 == bank.deposit(200);
        assert 200 == bank.getBalance();

        assert 30 == bank.withdraw(30);
        assert 170 == bank.getBalance();

        assert 10 == bank.deposit(40);
        assert 100 == bank.withdraw(225);
        assert 100 == bank.getBalance();

        assert 20 == bank.deposit(20);
        assert 40 == bank.deposit(40);
        assert 20 == bank.deposit(60);
        assert 200 == bank.getBalance();
        assert 26 == bank.getOperationsCount();

        assert 70 == bank.withdraw(70);
        assert 130 == bank.getBalance();
        assert 40 == bank.withdraw(40);
        assert 20 == bank.deposit(20);
        assert 100 == bank.withdraw(100);
        assert 10 == bank.getBalance();

    }
}
