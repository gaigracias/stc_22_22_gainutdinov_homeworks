public class ATM {
    private double balance = 0;
    private double maxWithdrawBalance;
    private double maxBankBalance;
    private int operationsCount = 0;
    ATM(double maxWithdrawBalance, double maxBankBalance){
        this.maxWithdrawBalance = maxWithdrawBalance;
        this.maxBankBalance = maxBankBalance;
    }
    public double getBalance(){
        return balance;
    }
    public int getOperationsCount(){
        return operationsCount;
    }
    public double withdraw(double money){
        operationsCount++;
        double temp = balance;
        if (money <= maxWithdrawBalance && balance >= money){
            balance -= money;
            return money;
        } else if (money > maxBankBalance && balance >= maxBankBalance){
            balance -= maxWithdrawBalance;
            return maxWithdrawBalance;
        } else if (money > balance){
            double temp1 = money - balance;
            balance -= (money - temp1);
            return temp;
        } else if (money > maxWithdrawBalance && balance >= money){
            balance -= maxWithdrawBalance;
            return maxWithdrawBalance;
        }
        return balance;
    }
    public double deposit(double money){
        operationsCount++;
        double temp = balance + money;
        if(temp > maxBankBalance){
            double a = temp - maxBankBalance;
            money -= a;
            balance += money;
            return a;
        }
        balance += money;
        return money;
    }
}
