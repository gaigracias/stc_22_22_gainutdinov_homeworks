import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

public interface CarsRepository {
    List<String> findAllCarNumByColorOrWithNoMileage(CarColor colors);

    HashMap<String, Integer> uniqueCountOfModelsByPrice(int priceFrom, int priceTo);

    Car getColorOfCarWithMinPrice(Consumer<Car> consumer);

    int averagePriceOfCar(String name);
}
