public enum CarColor {
    BLACK,
    GREEN,
    RED,
    GRAY,
    YELLOW,
    WHITE,
    BLUE
}
