import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private String fileName;

    CarsRepositoryFileBasedImpl(String fileName){
        this.fileName = fileName;
    }

    @Override
    public List<String> findAllCarNumByColorOrWithNoMileage(CarColor color) {
        return loadCarFile().stream()
                .filter(car -> car.getColor() == color || car.getMileage() == 0)
                .map(Car::getNum)
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public HashMap<String, Integer> uniqueCountOfModelsByPrice(int priceFrom, int priceTo) {
        List<String> stringList = new ArrayList<>();
        loadCarFile().stream()
                .filter(car -> car.getPrice() >= priceFrom && car.getPrice() <= priceTo)
                .forEach(car -> stringList.add(car.getModel()));

        return stringList.stream()
                .collect(Collectors.toMap(car -> car, car -> 1, Integer::sum, HashMap::new));
    }

    @Override
    public Car getColorOfCarWithMinPrice(Consumer<Car> consumer) {
        Car car = loadCarFile().stream()
                .distinct()
                .min(Comparator.comparingInt(Car::getPrice))
                .orElseThrow();
        consumer.accept(car);
        return car;
    }

    @Override
    public int averagePriceOfCar(String name) {
        return (int) loadCarFile().stream()
                .filter(car -> car.getModel().equals(name)).distinct()
                .mapToInt(Car::getPrice).average().orElseThrow();

    }

    private List<Car> loadCarFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(this::parseCar).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Car parseCar(String line) {
        String[] asSplitData = line.split("\\|");
        return new Car(
                asSplitData[0],
                asSplitData[1],
                CarColor.valueOf(asSplitData[2].toUpperCase()),
                Integer.parseInt(asSplitData[3]),
                Integer.parseInt(asSplitData[4]));
    }

}
