public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");

        assert "[o002aa111, o003aa111, o007aa111, o009aa111, o012aa111, o013aa111, o015aa111]".equals(carsRepository.findAllCarNumByColorOrWithNoMileage(CarColor.GREEN).toString());
        assert "{Toyota=2, Audi=7, Ford=1, Camry=2}".equals(carsRepository.uniqueCountOfModelsByPrice(700000, 800000).toString());
        assert "[o002aa111, Camry, green, 133, 0]".equals(carsRepository.getColorOfCarWithMinPrice(car -> System.out.println(car.getColor().toString())).toString());
        assert 269166 == carsRepository.averagePriceOfCar("Camry");

        assert "[o003aa111, o009aa111, o011aa111, o012aa111, o015aa111, o017aa111, o018aa111]".equals(carsRepository.findAllCarNumByColorOrWithNoMileage(CarColor.GRAY).toString());
        assert "{Tesla=1, Hyundai=2, BMW=1}".equals(carsRepository.uniqueCountOfModelsByPrice(10000, 50000).toString());
        assert 758571 == carsRepository.averagePriceOfCar("Audi");

    }
}
