public class Car {
    private String num;
    private String model;
    private CarColor color;
    private int mileage;
    private int price;

    public Car(String num, String model, CarColor color, int mileage, int price) {
        this.num = num;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNum() {
        return num;
    }

    public String getModel() {
        return model;
    }

    public CarColor getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return "[" + num
                + ", " + model
                + ", " + color.toString().toLowerCase()
                + ", " + mileage
                + ", " + price + "]";
    }
}
