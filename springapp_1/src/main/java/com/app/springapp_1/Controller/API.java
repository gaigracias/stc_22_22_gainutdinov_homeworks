package com.app.springapp_1.Controller;

import com.app.springapp_1.Model.User;
import com.app.springapp_1.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class API {
    @Autowired
    private UserRepository userRepository;

    @GetMapping(value = "/")
    public String get(){
        return "hello guys";
    }

    @GetMapping(value = "/users")
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @PostMapping(value = "/save")
    public String saveUser(@RequestBody User user){
        userRepository.save(user);
        return "new user saved";
    }

    @PutMapping(value = "/update/{id}")
    public String updateUser(@PathVariable long id, @RequestBody User user){
        User uUser = userRepository.findById(id).get();
        uUser.setName(user.getName());
        uUser.setAge(user.getAge());
        userRepository.save(uUser);
        return "user updated";
    }
    @DeleteMapping(value = "/delete/{id}")
    public String deleteUser(@PathVariable long id){
        User dUser = userRepository.findById(id).get();
        userRepository.delete(dUser);
        return "user deleted";
    }
}
