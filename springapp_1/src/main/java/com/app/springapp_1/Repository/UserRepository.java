package com.app.springapp_1.Repository;

import com.app.springapp_1.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
