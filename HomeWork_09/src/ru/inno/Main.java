package ru.inno;

public class Main {

    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        assert null == arrayList.get(0);
        assert 0 == arrayList.size();
        arrayList.add(4);
        assert 4 == arrayList.get(0);
        assert 1 == arrayList.size();
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(2);
        arrayList.add(7);
        assert 4 == arrayList.get(1);
        assert 5 == arrayList.get(2);
        assert 2 == arrayList.get(3);
        assert 7 == arrayList.get(4);
        assert 5 == arrayList.size();
        arrayList.remove(2);
        arrayList.remove(0);
        assert 4 == arrayList.get(0);
        assert 4 == arrayList.get(1);
        assert 5 == arrayList.get(2);
        assert 7 == arrayList.get(3);
        assert 4 == arrayList.size();
        arrayList.remove(7);
        assert 3 == arrayList.size();
        arrayList.removeAt(0);
        arrayList.removeAt(2);
        assert 4 == arrayList.get(0);
        assert 5 == arrayList.get(1);
        assert 2 == arrayList.size();
        arrayList.removeAt(1);
        assert 4 == arrayList.get(0);
        assert 1 == arrayList.size();
        arrayList.add(4);
        arrayList.add(5);
        arrayList.remove(4);
        assert 4 == arrayList.get(0);
        assert 5 == arrayList.get(1);
        assert 2 == arrayList.size();
        assert null == arrayList.get(2);



        List<Integer> linkedList = new LinkedList<>();
        assert null == linkedList.get(0);
        assert 0 == linkedList.size();
        linkedList.add(4);
        linkedList.add(4);
        assert 2 == linkedList.size();
        assert 4 == linkedList.get(0);
        assert null == linkedList.get(2);
        linkedList.add(2);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.remove(2);
        assert 4 == linkedList.get(0);
        assert 4 == linkedList.get(1);
        assert 2 == linkedList.get(2);
        assert 3 == linkedList.get(3);
        assert 4 == linkedList.size();
        linkedList.removeAt(0);
        linkedList.removeAt(2);
        assert 4 == linkedList.get(0);
        assert 2 == linkedList.get(1);



        List<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("hello 1");
        stringArrayList.add("hello 2");
        stringArrayList.add("hello 3");
        stringArrayList.add("hello 4");
        stringArrayList.remove("hello 2");
        stringArrayList.removeAt(2);
        assert "[hello 1, hello 3]".equals(stringArrayList.toString());
        assert 2 == stringArrayList.size();
        stringArrayList.add("hello 5");
        stringArrayList.add("hello 6");
        stringArrayList.add("hello 7");
        stringArrayList.add("hello 8");
        stringArrayList.add("hello 9");
        stringArrayList.add("hello 10");
        stringArrayList.add("hello 11");
        stringArrayList.add("hello 12");
        stringArrayList.add("hello 13");
        stringArrayList.add("hello 14");
        assert "[hello 1, hello 3, hello 5, hello 6, hello 7, hello 8, hello 9, hello 10, hello 11, hello 12, hello 13, hello 14]".equals(stringArrayList.toString());
        assert 12 == stringArrayList.size();
        stringArrayList.remove("hello 7");
        stringArrayList.remove("hello 11");
        stringArrayList.remove("hello 5");
        stringArrayList.removeAt(8);
        stringArrayList.removeAt(7);
        stringArrayList.removeAt(0);
        assert "[hello 3, hello 6, hello 8, hello 9, hello 10, hello 12]".equals(stringArrayList.toString());
        assert 6 == stringArrayList.size();
        stringArrayList.removeAt(0);
        stringArrayList.removeAt(0);
        stringArrayList.removeAt(0);
        stringArrayList.remove("hello 9");
        stringArrayList.remove("hello 10");
        stringArrayList.remove("hello 12");
        assert "[]".equals(stringArrayList.toString());
        assert 0 == stringArrayList.size();

    }
}