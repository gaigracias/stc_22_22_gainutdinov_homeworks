import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String text = "Hello Hello bye Hello bye Inno";
        Map.Entry<String, Integer> entry = getMax(text);
        assert "Hello".equals(entry.getKey());
        assert 3 == entry.getValue();

        String text1 = "Bin Bin Road Tip Space Road Tip Unique Tip Tip";
        Map.Entry<String, Integer> entry1 = getMax(text1);
        assert "Tip".equals(entry1.getKey());
        assert 4 == entry1.getValue();

    }
    public static Map.Entry<String, Integer> getMax(String text){
        List<String> stringList = Arrays.asList(text.split(" "));
        HashMap<String, Integer> hashMap = stringList.stream()
                .collect(Collectors.toMap(e->e, e->1, (a,b) -> Integer.sum(a, b), () -> new HashMap<>()));

        return Collections.max(hashMap.entrySet(), Map.Entry.comparingByValue());
    }
}
