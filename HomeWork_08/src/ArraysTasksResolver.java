import java.util.Arrays;

public class ArraysTasksResolver {
    static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println("from "+from+" to "+to);
        System.out.println(task.resolve(array, from, to)+"\n");
    }
    static int testResolveTask(int[] array, ArrayTask task, int from, int to) {
        return task.resolve(array, from, to);
    }
}
