public class Main {
    public static void main(String[] args){
        ArrayTask arrayAddition = (array, from, to) -> {
            int temp = 0;
            if (to > array.length){
                return 0;
            }
            for (int i = from; i <= to; i++){
                temp += array[i];
            }
            return temp;
        };
        ArrayTask arrayMaxValueAddition = (array, from, to) -> {
            int temp = array[from];
            if (to > array.length){
                return 0;
            }
            for (int i = from; i <= to; i++){
                if (array[i] > temp){
                    temp = array[i];
                }
            }
            int res = 0;
            if (!(temp > 0)){
                res = temp;
            }
            while (temp > 0){
                int digit = temp % 10;
                res += digit;
                temp /= 10;
            }
            return res;
        };

        int[] arr = {12, 62, 4, 2, 100, 40, 56};

        ArraysTasksResolver.resolveTask(arr, arrayAddition, 1, 3);
        ArraysTasksResolver.resolveTask(arr, arrayMaxValueAddition, 1, 3);

        assert 68 == ArraysTasksResolver.testResolveTask(arr, arrayAddition, 1, 3);
        assert 8 == ArraysTasksResolver.testResolveTask(arr, arrayMaxValueAddition, 1, 3);
        assert 202 == ArraysTasksResolver.testResolveTask(arr, arrayAddition, 2, 6);
        assert 1 == ArraysTasksResolver.testResolveTask(arr, arrayMaxValueAddition, 2, 6);
        assert 4+2+100 == ArraysTasksResolver.testResolveTask(arr, arrayAddition, 2, 4);

        int[] arr1 = {122, 623, 4, -2, 1100, 40, 56, 3, 8, 2, 0, -1, 44, 2, 123, 23, 65, 2, 4, 2};

        assert 8 == ArraysTasksResolver.testResolveTask(arr1, arrayAddition, 17, 19);
        assert 4 == ArraysTasksResolver.testResolveTask(arr1, arrayMaxValueAddition, 17, 19);
        assert 623+4+(-2)+1100 == ArraysTasksResolver.testResolveTask(arr1, arrayAddition, 1, 4);
        assert 2 == ArraysTasksResolver.testResolveTask(arr1, arrayMaxValueAddition, 1, 4);
        assert 11 == ArraysTasksResolver.testResolveTask(arr1, arrayMaxValueAddition, 5, 7);
        assert 99 == ArraysTasksResolver.testResolveTask(arr1, arrayAddition, 5, 7);

        assert -1 == ArraysTasksResolver.testResolveTask(arr1, arrayAddition, 10, 11);
        assert 0 == ArraysTasksResolver.testResolveTask(arr1, arrayMaxValueAddition, 10, 11);

        int[] arr2 = {-1, -1, 2, 0, 0, 2, 2, 2, 2, 0, -10};

        assert -2 == ArraysTasksResolver.testResolveTask(arr2, arrayAddition, 0, 1);
        assert -1 == ArraysTasksResolver.testResolveTask(arr2, arrayMaxValueAddition, 0, 1);

        assert 0 == ArraysTasksResolver.testResolveTask(arr2, arrayAddition, 0, 2);
        assert 2 == ArraysTasksResolver.testResolveTask(arr2, arrayMaxValueAddition, 0, 2);

        assert 4 == ArraysTasksResolver.testResolveTask(arr2, arrayAddition, 3, 6);
        assert 2 == ArraysTasksResolver.testResolveTask(arr2, arrayMaxValueAddition, 3, 6);

        assert -6 == ArraysTasksResolver.testResolveTask(arr2, arrayAddition, 7, 10);
        assert 2 == ArraysTasksResolver.testResolveTask(arr2, arrayMaxValueAddition, 7, 10);

        assert -10 == ArraysTasksResolver.testResolveTask(arr2, arrayAddition, 9, 10);
        assert 0 == ArraysTasksResolver.testResolveTask(arr2, arrayMaxValueAddition, 9, 10);

        assert 0 == ArraysTasksResolver.testResolveTask(arr2, arrayAddition, 3, 4);
        assert 0 == ArraysTasksResolver.testResolveTask(arr2, arrayMaxValueAddition, 3, 4);

    }
}
