package ru.inno;

import ru.inno.cars.models.CarColors;
import ru.inno.cars.repository.CarRepository;
import ru.inno.cars.repository.CarRepositoryJdbcImpl;

import java.util.Scanner;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        var property = System.getProperty("action");

        CarRepository carRepository = new CarRepositoryJdbcImpl();

        if (property.equals("read")){
            carRepository.toTable(carRepository.findAll());
        } else if (property.equals("write")) {
            Function<String, Object> userDataHandler = text -> {
                System.out.println(text);
                return new Scanner(System.in).nextLine();
            };

            carRepository.insert(
                    userDataHandler.apply("Car name").toString(),
                    CarColors.valueOf(userDataHandler.apply("Car color").toString().toUpperCase()),
                    userDataHandler.apply("Car number").toString(),
                    userDataHandler.apply("Car owner name").toString()
               );

        } else {
            System.exit(0);
        }
    }
}