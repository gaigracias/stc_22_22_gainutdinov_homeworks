package ru.inno.cars.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Car {
    private Long id;
    private String modelName;
    private CarColors color;
    private String carNumber;
    private Integer carOwnerId;
}
