package ru.inno.cars.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataBase {
    private static Connection connection;

    public static Connection connection(){
        try {
            Properties properties = getProperties();
            connection = DriverManager.getConnection(
                    properties.getProperty("database.url"),
                    properties.getProperty("database.name"),
                    properties.getProperty("database.password"));
            return connection;
        } catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    private static Properties getProperties(){
        try {
             Properties properties = new Properties();
             properties.load(new BufferedReader(
                    new InputStreamReader(DataBase.class.getResourceAsStream("/db.properties"))));
             return properties;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
