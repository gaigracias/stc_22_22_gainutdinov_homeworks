package ru.inno.cars.repository;

import ru.inno.cars.app.DataBase;
import ru.inno.cars.models.Car;
import ru.inno.cars.models.CarColors;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CarRepositoryJdbcImpl implements CarRepository {
    @Override
    public List<Car> findAll() {
        String query = "SELECT * FROM car ORDER BY id";
        List<Car> cars = new ArrayList<>();
        try (PreparedStatement stmt = DataBase.connection().prepareStatement(query)){
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Car car = Car.builder()
                        .id(resultSet.getLong("id"))
                        .modelName(resultSet.getString("model_name"))
                        .color(CarColors.valueOf(resultSet.getString("color").toUpperCase()))
                        .carNumber(resultSet.getString("car_number"))
                        .carOwnerId(resultSet.getInt("owner_id"))
                        .build();

                cars.add(car);
            }
            return cars;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insert(String modelName, CarColors color, String carNumber, String owner) {
        String query = "INSERT INTO car (model_name, color, car_number, owner_id) VALUES (?, ?, ?, (SELECT id FROM driver WHERE first_name LIKE ?))";
        try (PreparedStatement stmt = DataBase.connection().prepareStatement(query)){
            stmt.setString(1, modelName);
            stmt.setString(2, color.toString().toLowerCase());
            stmt.setString(3, carNumber);
            stmt.setString(4, "%"+owner+"%");

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void toTable(List<Car> cars) {
        Iterator<Car> carIterator = cars.iterator();

        Object[] header = {"id", "modelName", "color", "number", "owner_id"};
        System.out.format("%-15s%-15s%-15s%-15s%-15s%n", header);
        while (carIterator.hasNext()){
            Car car = carIterator.next();
            String[] carsData = {car.getId().toString(),
                    car.getModelName(),
                    car.getColor().toString(),
                    car.getCarNumber(),
                    car.getCarOwnerId() == 0 ? "NULL" : car.getCarOwnerId().toString()};
            System.out.format("%-15s%-15s%-15s%-15s%-15s%n", carsData);
        }
    }
}
