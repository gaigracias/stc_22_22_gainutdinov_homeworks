package ru.inno.cars.repository;

import ru.inno.cars.models.Car;
import ru.inno.cars.models.CarColors;

import java.util.List;

public interface CarRepository {
    List<Car> findAll();

    void insert(String modelName, CarColors color, String carNumber, String owner);

    void toTable(List<Car> cars);
}
