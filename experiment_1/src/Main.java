public class Main {
    public static void main(String[] args) {
        Data data = new User();
        System.out.println(data.getClass());

        String str = "";
        Class fromStrObject = str.getClass();
        Class clazz = String.class;
        System.out.println(fromStrObject);
        System.out.println(clazz);

        Class fromStrObject1 = data.getClass();
        Class clazz1 = Data.class;
        System.out.println(fromStrObject1);
        System.out.println(clazz1);
    }
}