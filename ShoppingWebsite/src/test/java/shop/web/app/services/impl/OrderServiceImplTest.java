package shop.web.app.services.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.web.app.models.Order;
import shop.web.app.models.Product;
import shop.web.app.models.User;
import shop.web.app.models.dto.OrderForm;
import shop.web.app.models.enums.OrderStatus;
import shop.web.app.repositories.OrderRepository;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderServiceImplTest {

    private OrderRepository orderRepository;
    private OrderServiceImpl orderService;

    private static final Long ORDER_ID = 2L;

    private static final Long PRODUCT_ID = 4L;

    private static final Long USER_ID = 5L;

    private static final User USER = User.builder()
            .id(USER_ID)
            .username("User 1")
            .build();

    private static final Order ORDER = Order.builder()
            .createdDate(LocalDate.now())
            .status(OrderStatus.ACTIVE)
            /*.products(Set.of(Product.builder()
                    .id(PRODUCT_ID)
                    .title("Product 1")
                    .price(4.2)
                    .quantity(2)
                    .build()))*/
            .build();

    private static final Order ORDER_WITH_ANOTHER_ARGUMENT = Order.builder()
            .createdDate(LocalDate.now())
            .status(OrderStatus.INACTIVE)
            .user(USER)
            .build();

    @BeforeEach
    void setUp() {
        this.orderRepository = Mockito.mock(OrderRepository.class);
        this.orderService = new OrderServiceImpl(orderRepository);

        when(orderRepository.findById(ORDER_ID)).thenReturn(Optional.of(ORDER));
    }

    @Test
    void add() {

        OrderForm order = new OrderForm();
        order.setCreatedDate(LocalDate.now());
        order.setUser(USER);

        orderService.add(order);
        verify(orderRepository).save(ORDER);
        //verify(orderRepository).save(ORDER_WITH_ANOTHER_ARGUMENT);

    }

    @Test
    void addWrongArgument() {

        OrderForm order = new OrderForm();
        order.setCreatedDate(LocalDate.of(2023, 12, 31));
        order.setStatus(OrderStatus.INACTIVE);
        order.setUser(USER);

        orderService.add(order);
        verify(orderRepository).save(ORDER);
    }

}