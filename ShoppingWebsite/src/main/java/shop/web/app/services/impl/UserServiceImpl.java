package shop.web.app.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import shop.web.app.models.User;
import shop.web.app.models.dto.UserForm;
import shop.web.app.repositories.UserRepository;
import shop.web.app.services.UserService;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void add(UserForm user) {
        User newUser = User.builder()
                .username(user.getUsername())
                .email(user.getEmail())
                .password(new BCryptPasswordEncoder().encode(user.getPassword()))
                .role(user.getRole())
                .build();

        userRepository.save(newUser);
    }

    @Override
    public void update(Long userId, UserForm user) {
        User update = userRepository.findById(userId).orElseThrow();

        update.setUsername(user.getUsername());
        update.setEmail(user.getEmail());
        update.setFirstName(user.getFirstName());
        update.setLastName(user.getLastName());
        update.setRole(user.getRole());

        userRepository.save(update);
    }
}
