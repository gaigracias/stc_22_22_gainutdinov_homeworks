package shop.web.app.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.web.app.models.Product;
import shop.web.app.models.dto.ProductForm;
import shop.web.app.repositories.CartRepository;
import shop.web.app.repositories.OrderRepository;
import shop.web.app.repositories.ProductRepository;
import shop.web.app.services.ProductService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

    private final OrderRepository orderRepository;

    private final CartRepository cartRepository;

    private final ProductRepository productRepository;

    @Override
    public void add(ProductForm product) {
        Product newProduct = Product.builder()
                .title(product.getTitle())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .category(product.getCategory())
                .build();

        productRepository.save(newProduct);
    }

    @Override
    public void update(Long productId, ProductForm product) {
        Product update = productRepository.findById(productId).orElseThrow();

        update.setTitle(product.getTitle());
        update.setPrice(product.getPrice());
        update.setQuantity(product.getQuantity());
        update.setCategory(product.getCategory());

        productRepository.save(update);
    }

    @Override
    public void delete(Long productId) {
        Product product = productRepository.findById(productId).orElseThrow();
        cartRepository.deleteAll(cartRepository.findAllByProductsContains(product));
        orderRepository.deleteAll(orderRepository.findAllByProductsContains(product));

        productRepository.deleteById(productId);
    }

}
