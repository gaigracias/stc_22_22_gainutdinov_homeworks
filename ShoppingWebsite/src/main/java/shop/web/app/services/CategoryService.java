package shop.web.app.services;

import shop.web.app.models.dto.CategoryForm;

public interface CategoryService {

    void add(CategoryForm category);

    void update(Long categoryId, CategoryForm category);
}
