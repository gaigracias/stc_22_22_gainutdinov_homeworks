package shop.web.app.services;

import shop.web.app.models.dto.UserForm;

public interface UserService {
    void add(UserForm user);

    void update(Long userId, UserForm user);
}
