package shop.web.app.services;

import shop.web.app.models.Cart;
import shop.web.app.models.dto.OrderForm;

public interface OrderService {

    void add(OrderForm order);

    void update(Long orderId, OrderForm order);
}
