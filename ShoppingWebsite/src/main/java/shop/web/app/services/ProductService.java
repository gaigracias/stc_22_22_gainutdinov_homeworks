package shop.web.app.services;

import shop.web.app.models.User;
import shop.web.app.models.dto.ProductForm;

public interface ProductService {
    void add(ProductForm product);

    void update(Long productId, ProductForm product);

    void delete(Long productId);
}
