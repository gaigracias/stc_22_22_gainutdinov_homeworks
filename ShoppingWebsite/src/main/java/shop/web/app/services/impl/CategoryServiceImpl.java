package shop.web.app.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.web.app.models.Category;
import shop.web.app.models.dto.CategoryForm;
import shop.web.app.repositories.CategoryRepository;
import shop.web.app.services.CategoryService;

@RequiredArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public void add(CategoryForm category) {
        Category newCategory = Category.builder()
                .title(category.getTitle())
                .build();

        categoryRepository.save(newCategory);
    }

    @Override
    public void update(Long categoryId, CategoryForm category) {
        Category update = categoryRepository.findById(categoryId).orElseThrow();

        update.setTitle(category.getTitle());

        categoryRepository.save(update);
    }
}
