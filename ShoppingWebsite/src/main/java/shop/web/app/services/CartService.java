package shop.web.app.services;

import shop.web.app.models.User;

public interface CartService {

    void addProduct(Long currentAuthorizedUserId, Long productId);

    void deleteItem(Long currentAuthorizedUserId, Long productId);

    void cartToOrder(User currentAuthorizedUser);
}
