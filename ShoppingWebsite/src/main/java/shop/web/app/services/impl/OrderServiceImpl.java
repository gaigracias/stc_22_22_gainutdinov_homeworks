package shop.web.app.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.web.app.models.Order;
import shop.web.app.models.dto.OrderForm;
import shop.web.app.models.enums.OrderStatus;
import shop.web.app.repositories.OrderRepository;
import shop.web.app.services.OrderService;

@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Override
    public void add(OrderForm order) {
        Order newOrder = Order.builder()
                .createdDate(order.getCreatedDate())
                .status(OrderStatus.ACTIVE)
                .user(order.getUser())
                .build();

        orderRepository.save(newOrder);
    }

    @Override
    public void update(Long orderId, OrderForm order) {
        Order update = orderRepository.findById(orderId).orElseThrow();

        update.setCreatedDate(order.getCreatedDate());
        update.setStatus(order.getStatus());
        update.setUser(order.getUser());

        orderRepository.save(update);
    }
}
