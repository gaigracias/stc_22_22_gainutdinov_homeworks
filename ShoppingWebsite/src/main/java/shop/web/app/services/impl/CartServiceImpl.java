package shop.web.app.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.web.app.models.Cart;
import shop.web.app.models.Order;
import shop.web.app.models.User;
import shop.web.app.models.dto.OrderForm;
import shop.web.app.repositories.CartRepository;
import shop.web.app.repositories.OrderRepository;
import shop.web.app.repositories.ProductRepository;
import shop.web.app.repositories.UserRepository;
import shop.web.app.services.CartService;
import shop.web.app.services.OrderService;

import java.time.LocalDate;
import java.util.HashSet;

@RequiredArgsConstructor
@Service
public class CartServiceImpl implements CartService {

    private final ProductRepository productRepository;

    private final UserRepository userRepository;

    private final CartRepository cartRepository;

    private final OrderRepository orderRepository;
    private final OrderService orderService;

    @Override
    public void addProduct(Long currentAuthorizedUserId, Long productId) {
        Cart cart = checkCartBeforeAddOrDelete(currentAuthorizedUserId);
        cart.getProducts().add(productRepository.findById(productId).orElseThrow());
        cartRepository.save(cart);
    }

    private Cart checkCartBeforeAddOrDelete(Long currentAuthorizedUserId) {
        User user = userRepository.findById(currentAuthorizedUserId).orElseThrow();
        Cart cart = cartRepository.findByCartUser(user);

        if (cart == null){
            Cart newCart = Cart.builder()
                    .cartUser(user)
                    .build();

            cartRepository.save(newCart);
            cart = cartRepository.findByCartUser(user);

        }
        if (cart.getProducts() == null){
            cart.setProducts(new HashSet<>());
        }

        return cart;
    }

    @Override
    public void deleteItem(Long currentAuthorizedUserId, Long productId) {
        Cart cart = checkCartBeforeAddOrDelete(currentAuthorizedUserId);
        cart.getProducts().remove(productRepository.findById(productId).orElseThrow());
        cartRepository.save(cart);
    }

    @Override
    public void cartToOrder(User currentAuthorizedUser) {
        User user = userRepository.findById(currentAuthorizedUser.getId()).orElseThrow();
        Cart cart = cartRepository.findByCartUser(user);

        OrderForm orderForm = new OrderForm();
        orderForm.setCreatedDate(LocalDate.now());
        orderForm.setUser(user);
        orderService.add(orderForm);

        Order order = orderRepository.findFirstByUserOrderByIdDesc(currentAuthorizedUser).orElseThrow();
        order.setProducts(new HashSet<>());
        order.getProducts().addAll(cart.getProducts());
        orderRepository.save(order);
    }
}
