package shop.web.app.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import shop.web.app.models.Category;
import shop.web.app.models.Product;
import shop.web.app.models.dto.ProductForm;
import shop.web.app.repositories.CategoryRepository;
import shop.web.app.repositories.ProductRepository;
import shop.web.app.security.details.CustomUserDetails;
import shop.web.app.services.ProductService;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Controller
public class ProductController {

    private final ProductRepository productRepository;
    private final ProductService productService;

    private final CategoryRepository categoryRepository;

    @GetMapping("/products")
    public String getProducts(@AuthenticationPrincipal CustomUserDetails userDetails,
                              @RequestParam Optional<Integer> page,
                              @RequestParam(required = false) Long categoryId,
            @RequestParam Optional<String> sortBy, Model model) {
        Pageable pageable = PageRequest.of(
                page.orElse(0),
                5,
                Sort.Direction.ASC, sortBy.orElse("id"));

        List<Product> products;
        if (categoryId == null) {
            products = productRepository.findAll(pageable).stream().toList();
            model.addAttribute("categoryParamId", "");
        } else {
            Category category = categoryRepository.findById(categoryId).orElseThrow();
            products = productRepository.findAllByCategory(category, pageable);
            String categoryParam = "&categoryId=" + categoryId;
            model.addAttribute("categoryParamId", categoryParam);
        }

        if (userDetails != null){
            model.addAttribute("currentUser", userDetails.getUser());
        }
        model.addAttribute("products", products);
        model.addAttribute("productsPage", pageable);
        model.addAttribute("categories", categoryRepository.findAll());

        return "view/products";
    }

    @GetMapping("/product/{id}")
    public String getProductPage(@PathVariable("id") Long productId, Model model) {
        model.addAttribute("product", productRepository.findById(productId).orElseThrow());
        return "view/product";
    }

    @GetMapping("/products/add")
    public String getProductAddPage(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        return "view/product_add";
    }

    @PostMapping("/products/add")
    public String addProduct(@RequestParam("category-id") Long categoryId, ProductForm product) {
        product.setCategory(categoryRepository.findById(categoryId).orElseThrow());
        productService.add(product);
        return "redirect:/products";
    }

    @GetMapping("/product/{id}/edit")
    public String getProductUpdatePage(@PathVariable("id") Long productId, Model model) {
        model.addAttribute("product", productRepository.findById(productId).orElseThrow());
        model.addAttribute("categories", categoryRepository.findAll());
        return "view/product_edit";
    }

    @PostMapping("/product/{id}/edit")
    public String updateProduct(@PathVariable("id") Long productId, @RequestParam("category-id") Long categoryId,
                                ProductForm product) {
        product.setCategory(categoryRepository.findById(categoryId).orElseThrow());
        productService.update(productId, product);
        return "redirect:/products";
    }

    @PostMapping("/product/{id}/delete")
    public String deleteProduct(@PathVariable("id") Long id) {
        productService.delete(id);
        return "redirect:/products";
    }

}
