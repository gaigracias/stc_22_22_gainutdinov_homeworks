package shop.web.app.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.web.app.models.dto.CategoryForm;
import shop.web.app.repositories.CategoryRepository;
import shop.web.app.services.CategoryService;

import java.util.Optional;

@RequiredArgsConstructor
@Controller
public class CategoryController {

    private final CategoryRepository categoryRepository;
    private final CategoryService categoryService;

    @GetMapping("/categories")
    public String getCategories(@RequestParam Optional<Integer> page,
                           @RequestParam Optional<String> sortBy, Model model) {
        Pageable pageable = PageRequest.of(
                page.orElse(0),
                5,
                Sort.Direction.ASC, sortBy.orElse("id"));

        model.addAttribute("categories", categoryRepository.findAll(pageable).stream().toList());
        model.addAttribute("categoriesPage", pageable);
        return "view/categories";
    }

    @GetMapping("/category/add")
    public String getCategoryAddPage() {
        return "view/category_add";
    }

    @PostMapping("/category/add")
    public String addCategory(CategoryForm category) {
        categoryService.add(category);
        return "redirect:/categories";
    }

    @GetMapping("/category/{id}/edit")
    public String getCategoryUpdatePage(@PathVariable("id") Long categoryId, Model model) {
        model.addAttribute("category", categoryRepository.findById(categoryId).orElseThrow());
        return "view/category_edit";
    }

    @PostMapping("category/{id}/edit")
    public String updateCategory(@PathVariable("id") Long categoryId, CategoryForm category) {
        categoryService.update(categoryId, category);
        return "redirect:/categories";
    }

    @PostMapping("category/{id}/delete")
    public String deleteCategory(@PathVariable("id") Long id) {
        categoryRepository.deleteById(id);
        return "redirect:/categories";
    }
}
