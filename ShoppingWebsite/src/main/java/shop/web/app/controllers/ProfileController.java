package shop.web.app.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import shop.web.app.models.Cart;
import shop.web.app.models.User;
import shop.web.app.models.enums.OrderStatus;
import shop.web.app.repositories.CartRepository;
import shop.web.app.repositories.OrderRepository;
import shop.web.app.repositories.UserRepository;
import shop.web.app.security.details.CustomUserDetails;

@RequiredArgsConstructor
@Controller
public class ProfileController {

    private final UserRepository userRepository;

    private final CartRepository cartRepository;

    private final OrderRepository orderRepository;

    @GetMapping("/profile")
    public String getHomePage(@AuthenticationPrincipal CustomUserDetails userDetails, Model model) {
        User user = userRepository.findById(userDetails.getUser().getId()).orElseThrow();
        Cart cart = cartRepository.findByCartUser(user);

        if (cart == null){
            Cart newCart = Cart.builder()
                    .cartUser(user)
                    .build();

            cartRepository.save(newCart);
            cart = cartRepository.findByCartUser(user);
        }

        model.addAttribute("orders", orderRepository.findAllByUserAndStatusOrderByIdDesc(user, OrderStatus.ACTIVE));
        model.addAttribute("currentUser", user);
        model.addAttribute("cart", cart);
        return "view/profile";
    }
}
