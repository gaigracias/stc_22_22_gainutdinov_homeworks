package shop.web.app.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import shop.web.app.security.details.CustomUserDetails;
import shop.web.app.services.CartService;

@RequiredArgsConstructor
@Controller
public class CartController {

    private final CartService cartService;

    @PostMapping("/cart/{id}")
    public String addProductToUserCart(@AuthenticationPrincipal CustomUserDetails userDetails,
                                       @PathVariable("id") Long productId) {
        cartService.addProduct(userDetails.getUser().getId(), productId);
        return "redirect:/products";
    }

    @PostMapping("/profile/cart/{id}")
    public String deleteItemFromCart(@AuthenticationPrincipal CustomUserDetails userDetails,
                                     @PathVariable("id") Long productId) {
        cartService.deleteItem(userDetails.getUser().getId(), productId);
        return "redirect:/profile";
    }

    @PostMapping("/cart/newOrder")
    public String addCartToOrder(@AuthenticationPrincipal CustomUserDetails userDetails) {
        cartService.cartToOrder(userDetails.getUser());
        return "redirect:/profile";
    }

}
