package shop.web.app.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.web.app.models.Order;
import shop.web.app.models.User;
import shop.web.app.models.dto.OrderForm;
import shop.web.app.models.enums.OrderStatus;
import shop.web.app.repositories.OrderRepository;
import shop.web.app.repositories.UserRepository;
import shop.web.app.services.OrderService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Controller
public class OrderController {

    private final OrderRepository orderRepository;
    private final OrderService orderService;

    private final UserRepository userRepository;

    @GetMapping("/orders")
    public String getOrders(@RequestParam Optional<Integer> page,
                           @RequestParam Optional<String> sortBy, Model model) {
        Pageable pageable = PageRequest.of(
                page.orElse(0),
                5,
                Sort.Direction.ASC, sortBy.orElse("id"));
        List<Order> orders = orderRepository.findAll(pageable).stream().toList();

        model.addAttribute("orders", orders);
        model.addAttribute("ordersPage", pageable);
        return "view/orders";
    }

    @GetMapping("/order/{id}")
    public String getOrderPage(@PathVariable("id") Long orderId, Model model) {
        Order order = orderRepository.findById(orderId).orElseThrow();
        model.addAttribute("order", order);
        return "view/order";
    }

    @GetMapping("/order/add")
    public String getOrderAddPage(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "view/order_add";
    }

    @PostMapping("/order/add")
    public String addOrder(@RequestParam("user-id") User user, OrderForm order) {
        order.setUser(user);
        orderService.add(order);
        return "redirect:/orders";
    }

    @GetMapping("/order/{id}/edit")
    public String getOrderUpdatePage(@PathVariable("id") Long orderId, Model model) {
        model.addAttribute("order", orderRepository.findById(orderId).orElseThrow());
        model.addAttribute("orderStatus", Arrays.stream(OrderStatus.values()).toList());
        model.addAttribute("users", userRepository.findAll());
        return "view/order_edit";
    }

    @PostMapping("/order/{id}/edit")
    public String updateOrder(@PathVariable("id") Long orderId,
                             @RequestParam("user-id") User user,
                              @RequestParam("orderStatus") OrderStatus orderStatus, OrderForm order) {
        order.setUser(user);
        order.setStatus(orderStatus);
        orderService.update(orderId, order);
        return "redirect:/orders";
    }

    @PostMapping("/order/{id}/delete")
    public String deleteOrder(@PathVariable("id") Long id) {
        orderRepository.deleteById(id);
        return "redirect:/orders";
    }
}
