    package shop.web.app.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.web.app.models.Order;
import shop.web.app.models.User;
import shop.web.app.models.dto.UserForm;
import shop.web.app.models.enums.OrderStatus;
import shop.web.app.models.enums.UserRole;
import shop.web.app.repositories.OrderRepository;
import shop.web.app.repositories.UserRepository;
import shop.web.app.services.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Controller
public class UserController {

    private final UserRepository userRepository;
    private final UserService userService;

    private final OrderRepository orderRepository;


    @GetMapping("/users")
    public String getUsers(@RequestParam Optional<Integer> page,
            @RequestParam Optional<String> sortBy, Model model) {
        Pageable pageable = PageRequest.of(
                page.orElse(0),
                5,
                Sort.Direction.ASC, sortBy.orElse("id"));

        model.addAttribute("users", userRepository.findAll(pageable).stream().toList());
        model.addAttribute("usersPage", pageable);
        return "view/users";
    }

    @GetMapping("/user/{id}")
    public String getUserPage(@PathVariable("id") Long userId, Model model) {
        User user = userRepository.findById(userId).orElseThrow();
        List<Order> orders = orderRepository.findAllByUserAndStatusOrderByIdDesc(user, OrderStatus.ACTIVE);

        model.addAttribute("user", user);
        model.addAttribute("orders", orders);
        return "view/user";
    }

    @GetMapping("/user/add")
    public String getUserAddPage(Model model) {
        model.addAttribute("userRoles", Arrays.stream(UserRole.values()).toList());
        return "view/user_add";
    }

    @PostMapping("/user/add")
    public String addUser(@RequestParam("role") UserRole role, UserForm user) {
        user.setRole(role);
        userService.add(user);
        return "redirect:/users";
    }

    @GetMapping("/user/{id}/edit")
    public String getUserUpdatePage(@PathVariable("id") Long userId, Model model) {
        User user = userRepository.findById(userId).orElseThrow();
        model.addAttribute("user", user);
        model.addAttribute("userRoles", Arrays.stream(UserRole.values()).toList());
        return "view/user_edit";
    }

    @PostMapping("/user/{id}/edit")
    public String updateUser(@PathVariable("id") Long userId,
                                @RequestParam("role") UserRole role, UserForm user) {
        user.setRole(role);
        userService.update(userId, user);
        return "redirect:/users";
    }

    @PostMapping("/user/{id}/delete")
    public String deleteUser(@PathVariable("id") Long id) {
        userRepository.deleteById(id);
        return "redirect:/users";
    }

}
