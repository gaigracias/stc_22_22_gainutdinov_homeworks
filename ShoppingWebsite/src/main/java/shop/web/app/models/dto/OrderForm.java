package shop.web.app.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import shop.web.app.models.Product;
import shop.web.app.models.User;
import shop.web.app.models.enums.OrderStatus;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderForm {
    private LocalDate createdDate;
    private OrderStatus status;
    private User user;
    private Set<Product> products;
}
