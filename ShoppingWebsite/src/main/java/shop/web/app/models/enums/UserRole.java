package shop.web.app.models.enums;

public enum UserRole {
    USER, ADMIN
}
