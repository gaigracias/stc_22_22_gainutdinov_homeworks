package shop.web.app.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import shop.web.app.models.Category;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductForm {
    private String title;
    private Double price;
    private Integer quantity;
    private Category category;
}
