package shop.web.app.models.enums;

public enum OrderStatus {
    ACTIVE, INACTIVE
}
