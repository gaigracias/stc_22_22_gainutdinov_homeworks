package shop.web.app.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import shop.web.app.models.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartFrom {
    private User cartUser;
}
