package shop.web.app.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"orders", "category", "carts"})
@ToString(exclude = {"orders", "carts"})
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Double price;

    private Integer quantity;

    @ManyToMany(mappedBy = "products", fetch = FetchType.EAGER)
    private Set<Order> orders;

    @ManyToMany(mappedBy = "products", fetch = FetchType.EAGER)
    private Set<Cart> carts;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}
