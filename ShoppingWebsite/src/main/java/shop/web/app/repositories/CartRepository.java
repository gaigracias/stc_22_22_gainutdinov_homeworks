package shop.web.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.web.app.models.Cart;
import shop.web.app.models.Product;
import shop.web.app.models.User;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {
    Cart findByCartUser(User user);

    List<Cart> findAllByProductsContains(Product product);

}
