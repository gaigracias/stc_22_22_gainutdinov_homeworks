package shop.web.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.web.app.models.Order;
import shop.web.app.models.Product;
import shop.web.app.models.User;
import shop.web.app.models.enums.OrderStatus;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByUserAndStatusOrderByIdDesc(User user, OrderStatus orderStatus);

    Optional<Order> findFirstByUserOrderByIdDesc(User user);

    List<Order> findAllByProductsContains(Product product);

}
