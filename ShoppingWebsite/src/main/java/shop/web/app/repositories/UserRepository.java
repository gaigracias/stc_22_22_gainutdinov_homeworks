package shop.web.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.web.app.models.User;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
