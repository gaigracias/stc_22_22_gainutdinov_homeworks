package shapes;

public abstract class Shape {
    protected int x = 0;
    protected int y = 0;

    public abstract int getX();
    public abstract int getY();

    public abstract float perimeter();
    public abstract float area();
    public abstract void move(int x, int y);
}
