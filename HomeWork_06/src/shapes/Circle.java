package shapes;

public class Circle extends Shape{
    protected float radius;
    protected final float p = 3.14f;

    public Circle(float radius){
        this.radius = radius;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public float perimeter() {
        return 2 * p * radius;
    }

    @Override
    public float area() {
        return p * radius * radius;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
