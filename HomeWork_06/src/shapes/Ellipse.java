package shapes;

public class Ellipse extends Shape {
    protected float smallRadius;
    protected float bigRadius;
    protected final float p = 3.14f;

    public Ellipse(float smallRadius, float bigRadius){
        this.smallRadius = smallRadius;
        this.bigRadius = bigRadius;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public float perimeter() {
        return (int)(2 * p * (float)Math.sqrt((Math.pow(bigRadius, 2) + Math.pow(smallRadius, 2))/2));
    }

    @Override
    public float area() {
        return p * bigRadius * smallRadius;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
