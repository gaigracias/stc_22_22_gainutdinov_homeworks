package shapes;

public class Rectangle extends Shape {
    protected float width;
    protected float height;

    public Rectangle(float width, float height){
        this.width = width;
        this.height = height;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public float perimeter() {
        return 2 * (width + height);
    }

    @Override
    public float area() {
        return width * height;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
