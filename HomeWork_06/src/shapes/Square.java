package shapes;

public class Square extends Shape {
    protected float sideLength;

    public Square(float sideLength){
        this.sideLength = sideLength;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public float perimeter() {
        return 4 * sideLength;
    }

    @Override
    public float area() {
        return sideLength * sideLength;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
