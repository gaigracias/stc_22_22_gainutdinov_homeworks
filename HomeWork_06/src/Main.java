import shapes.*;

public class Main {
    public static void main(String[] args) {

        Shape square = new Square(6);
        assert 24 == square.perimeter();
        assert 36 == square.area();

        Shape rectangle = new Rectangle(30, 20);
        assert 100 == rectangle.perimeter();
        assert 600 == rectangle.area();

        Shape circle = new Circle(3);
        assert (float)18.84 == circle.perimeter();
        assert (float)28.26 == circle.area();

        Shape ellipse = new Ellipse(3, 6);
        assert 29 == ellipse.perimeter();
        assert (float)56.52 == ellipse.area();

        square.move(2, 5);
        assert 2 == square.getX();
        assert 5 == square.getY();
        rectangle.move(22, 53);
        assert 22 == rectangle.getX();
        assert 53 == rectangle.getY();
        circle.move(6, 6);
        assert 6 == circle.getX();
        assert 6 == circle.getY();
        ellipse.move(20, 15);
        assert 20 == ellipse.getX();
        assert 15 == ellipse.getY();

        Shape square1 = new Square(8);
        assert 32 == square1.perimeter();
        assert 64 == square1.area();

        Shape rectangle1 = new Rectangle(45, 10);
        assert 110 == rectangle1.perimeter();
        assert 450 == rectangle1.area();

        Shape circle1 = new Circle(4);
        assert (float)25.12 == circle1.perimeter();
        assert (float)50.24 == circle1.area();

        Shape ellipse1 = new Ellipse(7, 12);
        assert 61 == ellipse1.perimeter();
        assert (float)263.76 == ellipse1.area();

        Shape square2 = new Square(15);
        assert 60 == square2.perimeter();
        assert 225 == square2.area();

        Shape rectangle2 = new Rectangle(20, 50);
        assert 140 == rectangle2.perimeter();
        assert 1000 == rectangle2.area();

        Shape circle2 = new Circle(16);
        assert (float)100.48 == circle2.perimeter();
        assert (float)803.84 == circle2.area();

        Shape ellipse2 = new Ellipse(9, 17);
        assert 85 == ellipse2.perimeter();
        assert (float)480.42 == ellipse2.area();

        Shape square3 = new Square(40);
        assert 160 == square3.perimeter();
        assert 1600 == square3.area();

        Shape rectangle3 = new Rectangle(40, 40);
        assert 160 == rectangle3.perimeter();
        assert 1600 == rectangle3.area();

        Shape circle3 = new Circle(32);
        assert (float)200.96 == circle3.perimeter();
        assert (float)3215.36 == circle3.area();

        Shape ellipse3 = new Ellipse(32, 32);
        assert 200 == ellipse3.perimeter();
        assert (float)3215.36 == ellipse3.area();

    }
}
