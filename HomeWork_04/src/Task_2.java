public class Task_2 {
    public static void evenNumbers(int[] arr){
        for(int i = 0; i < arr.length; i++){
            if(arr[i] % 2 == 0){
                System.out.println(arr[i]);
            }
        }
    }
    public static void main(String[] args){
        int[] arr = {2,-4, 23, -3, 23, 13, 16, 8, 0, 1};
        evenNumbers(arr);
    }
}
