public class Task_3 {
    public static int toInt(int[] arr){
        String str = "";
        for (int i = 0; i < arr.length; i++){
            if (arr[i] < 0){
                return -1;
            }
            str += arr[i];
        }
        return Integer.parseInt(str);
    }
    public static void main(String[] args){
        int arr[] = {3,4,14,5,77};
        try{
            int result = toInt(arr);
            if (result == -1){
                System.out.println("It can't use negative numbers");
            }else {
                System.out.println(result);
            }

        }catch (Exception e){
            System.out.println("Int cannot be more than 2147483647");
        }
    }
}
