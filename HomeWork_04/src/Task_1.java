import java.util.Scanner;

public class Task_1 {
    public static int sumOfnumbers(int a, int b){
        int sum = 0;

        if (a > b){
            return -1;
        } else {
            int i = a;
            while (i < b){
                i++;
                a += i;
            }
            sum = a;
        }
        return sum;
    }
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter interval FROM:");
        int a = scanner.nextInt();
        System.out.println("Enter interval TO:");
        int b = scanner.nextInt();

        if (a > b){
            System.out.println("Interval FROM bigger than TO and it's equal to: "+sumOfnumbers(a,b));
        }else {
            System.out.println("SumOfInterval from "+a+" to "+b+" = "+sumOfnumbers(a,b));
        }
    }
}
