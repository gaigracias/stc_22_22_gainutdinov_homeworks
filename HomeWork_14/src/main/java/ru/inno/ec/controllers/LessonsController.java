package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.CoursesService;
import ru.inno.ec.services.LessonsService;

@RequiredArgsConstructor
@Controller
public class LessonsController {
    private final LessonsService lessonsService;
    private final LessonsRepository lessonsRepository;
    private final CoursesRepository coursesRepository;
    private final CoursesService coursesService;

    @GetMapping("/lessons/{id}")
    public String getLessonPage(@PathVariable("id") Long courseId, Model model) {
        model.addAttribute("lesson", lessonsRepository.findById(courseId).orElseThrow());
        return "view/lesson_page";
    }

    @GetMapping("/lessons")
    public String getLessonsPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("lessons", lessonsRepository.findAll());
        return "view/lessons_page";
    }

    @GetMapping("/lessons/add")
    public String getCoursesAdd(@RequestParam(value = "orderBy", required = false) String orderBy,
                             @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("courses", coursesService.findAll());
        return "view/lesson_add_page";
    }

    @PostMapping("/lessons/add/adding")
    public String add(LessonForm lesson) {
        //@RequestParam("course-id") Long courseId
        //lesson.setCourse(coursesRepository.findById(courseId).orElseThrow());
        lessonsService.add(lesson);
        return "redirect:/lessons/add";
    }

    @GetMapping("/lessons/edit")
    public String getCoursesEdit(@RequestParam(value = "orderBy", required = false) String orderBy,
                             @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("courses", coursesService.findAll());
        return "view/lesson_edit_page";
    }

    @GetMapping("/lessons/{id}/edit")
    public String getCourse(@PathVariable("id") Long lessonId, Model model) {
        model.addAttribute("courses", coursesService.findAll());
        model.addAttribute("lesson", lessonsRepository.findById(lessonId).orElseThrow());
        return "view/lesson_edit_page";
    }

    @PostMapping("/lessons/{id}/edit")
    public String editCourse(@PathVariable("id") Long lessonId, LessonForm lesson) {
        //@RequestParam("course-id") Long courseId
        //lesson.setCourse(coursesRepository.findById(courseId).orElseThrow());
        lessonsService.updateLesson(lessonId, lesson);
        return "redirect:/lessons/{id}/edit";
    }

    @PostMapping("/lessons/{id}/delete")
    public String delete(@PathVariable("id") Long id) {
        lessonsRepository.deleteById(id);
        return "redirect:/lessons";
    }
}
