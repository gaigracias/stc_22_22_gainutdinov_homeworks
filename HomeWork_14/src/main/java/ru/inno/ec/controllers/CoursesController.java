package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.CoursesService;
import ru.inno.ec.services.LessonsService;

@RequiredArgsConstructor
@Controller
public class CoursesController {

    private final CoursesService coursesService;
    private final CoursesRepository coursesRepository;

    @GetMapping("/courses")
    public String getCoursesPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                               @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("courses", coursesService.findAll());
        return "view/courses_page";
    }

    @PostMapping("/courses/{course-id}/students")
    public String addStudentToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("student-id") Long studentId) {
        coursesService.addStudentToCourse(courseId, studentId);
        return "redirect:/courses/" + courseId;
    }

    @PostMapping("/courses/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("lesson-id") Long lessonId) {
        coursesService.addLessonToCourse(courseId, lessonId);
        return "redirect:/courses/" + courseId;
    }

    @GetMapping("/courses/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(courseId));
        model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(courseId));
        model.addAttribute("notInCourseLessons", coursesService.getNotInCourseLessons(courseId));
        model.addAttribute("inCourseLessons", coursesService.getInCourseLessons(courseId));

        return "view/course_page";
    }

    @GetMapping("/courses/{id}/edit")
    public String getCourse(@PathVariable("id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        return "view/course_edit_page";
    }

    @PostMapping("/courses/{id}/edit")
    public String editCourse(@PathVariable("id") Long courseId, CourseForm course) {
        coursesService.updateCourse(courseId, course);
        return "redirect:/courses/{id}/edit";
    }

    // @DeleteMapping method returns 405 error
    @PostMapping("/courses/{id}/delete")
    public String delete(@PathVariable("id") Long id) {
        coursesRepository.deleteById(id);
        return "redirect:/courses";
    }

    @PostMapping("/courses/add/adding")
    public String add(CourseForm course) {
        coursesService.add(course);
        return "redirect:/courses/add";
    }

    @GetMapping("/courses/add")
    public String getAddPage() {
        return "view/course_add_page";
    }
}
