package ru.inno.ec.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.inno.ec.models.Course;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonForm {

    private String name;
    private String summary;
    private String startTime;
    private String finishTime;
    //private Course course;
}
