package ru.inno.ec.services;

import ru.inno.ec.dto.LessonForm;

public interface LessonsService {
    void add(LessonForm lesson);

    void updateLesson(Long lessonId, LessonForm lesson);

}
