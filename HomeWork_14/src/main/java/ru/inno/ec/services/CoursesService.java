package ru.inno.ec.services;

import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;

import java.util.List;

public interface CoursesService {

    List<Course> findAll();

    void add(CourseForm course);

    void addStudentToCourse(Long courseId, Long studentId);

    void addLessonToCourse(Long courseId, Long lessonId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);

    List<Lesson> getNotInCourseLessons(Long lessonId);

    List<Lesson> getInCourseLessons(Long lessonId);

    void updateCourse(Long courseId, CourseForm updateData);
}
