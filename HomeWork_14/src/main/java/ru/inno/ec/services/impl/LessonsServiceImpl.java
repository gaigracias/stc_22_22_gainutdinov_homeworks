package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.LessonsService;

import java.time.LocalTime;

@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {
    private final LessonsRepository lessonsRepository;

    @Override
    public void add(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .startTime(parseTime(lesson.getStartTime()))
                .finishTime(parseTime(lesson.getFinishTime()))
                //.course(lesson.getCourse())
                .build();

        lessonsRepository.save(newLesson);
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm lesson) {
        Lesson updateLesson = lessonsRepository.findById(lessonId).orElseThrow();

        updateLesson.setName(lesson.getName());
        updateLesson.setSummary(lesson.getSummary());
        updateLesson.setStartTime(parseTime(lesson.getStartTime()));
        updateLesson.setFinishTime(parseTime(lesson.getFinishTime()));
        //updateLesson.setCourse(lesson.getCourse());

        lessonsRepository.save(updateLesson);
    }

    private LocalTime parseTime(String time){
        return LocalTime.parse(time);
    }

}
