package numbers;

public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {
    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public int[] calc() {
        int i = from;
        int c = 0;
        int[] temp;
        while (i <= to){
            if (i % 2 == 0){
                c++;
            }
            i++;
        }
        i = from;
        temp = new int[c];
        c = 0;
        while (i <= to){
            if (i % 2 == 0){
                temp[c] = i;
                c++;
            }
            i++;
        }
        return temp;
    }
}
