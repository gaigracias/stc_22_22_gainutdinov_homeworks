import numbers.EvenNumbersPrintTask;
import numbers.OddNumbersPrintTask;
import numbers.Task;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Task[] tasks = {
                new EvenNumbersPrintTask(1, 8),
                new OddNumbersPrintTask(1, 9),
                new EvenNumbersPrintTask(1, 9),
                new EvenNumbersPrintTask(8, 24)
        };
        new Main().completeAllTasks(tasks);

        // Testing
        evenAndOddNumCheck(1, 18, tasks[0].calc(), 0);
        evenAndOddNumCheck(1, 9, tasks[1].calc(), 1);
        evenAndOddNumCheck(8, 56, tasks[3].calc(), 0);

        arraysEquality(tasks[0].calc(), tasks[2].calc());
        arraysEquality(tasks[1].calc(), oddCalc(1, 10));
        arraysEquality(tasks[2].calc(), evenCalc(1, 9));
        arraysEquality(tasks[3].calc(), evenCalc(8, 24));

    }
    void completeAllTasks(Task[] tasks){
        for (int i = 0; i < tasks.length; i++){
            tasks[i].complete();
        }
    }
    private static void evenAndOddNumCheck(int from, int to, int[] array, int check){
        int i = from;
        int c = 0;
        while (i <= to){
            if (i % 2 == 0 && check == 0){
                assert i == array[c];
                c++;
                if (c == array.length){
                    break;
                }
            }
            if (!(i % 2 == 0) && check == 1){
                assert i == array[c];
                c++;
                if (c == array.length){
                    break;
                }
            }
            i++;
        }
    }
    public static int[] evenCalc(int from, int to) {
        int i = from;
        int c = 0;
        int[] temp;
        while (i <= to){
            if (i % 2 == 0){
                c++;
            }
            i++;
        }
        i = from;
        temp = new int[c];
        c = 0;
        while (i <= to){
            if (i % 2 == 0){
                temp[c] = i;
                c++;
            }
            i++;
        }
        return temp;
    }
    public static int[] oddCalc(int from, int to) {
        int i = from;
        int c = 0;
        int[] temp;
        while (i <= to){
            if (!(i % 2 == 0)){
                c++;
            }
            i++;
        }
        i = from;
        temp = new int[c];
        c = 0;
        while (i <= to){
            if (!(i % 2 == 0)){
                temp[c] = i;
                c++;
            }
            i++;
        }
        return temp;
    }
    public static void arraysEquality(int[] a1, int[] a2) {
        assert Arrays.equals(a1, a2);
    }
}
