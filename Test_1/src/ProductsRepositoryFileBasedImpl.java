import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Product findById(Integer id) {
        return loadProductsFile().stream()
                .filter(p -> p.getId().equals(id))
                .findFirst()
                .get();
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        return loadProductsFile().stream()
                .filter(p -> p.getTitle().toLowerCase().contains(title.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public void update(Product product) {
        List<Product> products = loadProductsFile();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            products.forEach(p -> {
                try {
                    if (product.getId().equals(p.getId())) {
                        writer.write(toLine(product));
                    } else {
                        writer.write(toLine(p));
                    }
                    writer.newLine();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String toLine(Product product) {
        return product.getId()
                + "|" + product.getTitle()
                + "|" + product.getPrice()
                + "|" + product.getCount();
    }

    private List<Product> loadProductsFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(this::parseProduct).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Product parseProduct(String line) {
        String[] asSplitData = line.split("\\|");
        return new Product(
                Integer.parseInt(asSplitData[0]),
                asSplitData[1],
                Double.parseDouble(asSplitData[2]),
                Integer.parseInt(asSplitData[3]));
    }
}
