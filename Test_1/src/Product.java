public class Product {
    private Integer id;
    private String title;
    private double price;
    private int count;

    public Product(int id, String title, double price, int count) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        if (count == 0) {
            stringBuilder.append("]");
        } else {
            stringBuilder.append(getId()
                    + "|" + getTitle()
                    + "|" + getPrice()
                    + "|" + getCount() + "]");
        }
        return stringBuilder.toString();
    }
}