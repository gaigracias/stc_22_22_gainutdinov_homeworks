import java.util.List;

public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository =
                new ProductsRepositoryFileBasedImpl("products.txt");

        assert "[1|Молоко|39.8|1]".equals(productsRepository.findById(1).toString());
        assert "[3|Мясо|65.8|1]".equals(productsRepository.findById(3).toString());
        assert "[8|Броколли|26.2|4]".equals(productsRepository.findById(8).toString());
        assert "[17|Пакетик чайа|16.0|2]".equals(productsRepository.findById(17).toString());
        assert "[5|Соло|12.3|2]".equals(productsRepository.findById(5).toString());
        assert "[23|Собачий корм|72.5|1]".equals(productsRepository.findById(23).toString());

        List<Product> products = productsRepository.findAllByTitleLike("оЛо");
        assert "[[1|Молоко|39.8|1], [5|Соло|12.3|2]]".equals(products.toString());
        List<Product> products1 = productsRepository.findAllByTitleLike("йо");
        assert "[[4|Йогурт|23.8|2]]".equals(products1.toString());
        List<Product> products2 = productsRepository.findAllByTitleLike("Кор");
        assert "[[23|Собачий корм|72.5|1], [24|Кошачий корм|62.3|2]]".equals(products2.toString());

        Product solo = productsRepository.findById(5);
        solo.setCount(3);
        solo.setPrice(16);
        assert "[5|Соло|12.3|2]".equals(productsRepository.findById(5).toString());
        productsRepository.update(solo);
        assert "[5|Соло|16.0|3]".equals(productsRepository.findById(5).toString());

        Product bread = productsRepository.findById(2);
        bread.setCount(4);
        bread.setPrice(31);
        assert "[2|Хлеб|15.2|3]".equals(productsRepository.findById(2).toString());
        productsRepository.update(bread);
        assert "[2|Хлеб|31.0|4]".equals(productsRepository.findById(2).toString());

    }
}
